---
title: "Peach ice tea recipe"
date: 2023-11-24
tags:
  - cooking
---

The contents are for making 350ml peach ice tea.

![Peach ice tea](../../assets/images/blog/2023-11-24-peach-ice-tea-recipe/ice-tea.jpg)

## Ingredients

- Water - 100ml
- Cold water - 100ml
- Tea powder - 15g
- Peach - 1
- Honey - 15ml
- Fresh lemon juice - 15ml
- Mint (Optional) - 5 to 6 leaves
- Ice cubes

## Steps

- Boil tea powder in water for 2-3 minutes.
- Strain the tea and rest it till it comes to the room temperature.
- Make pulp from the peach with the help of mixer or blender.
- In a serving glass, mix peach pulp, tea infused water, honey, lemon juice, and crushed/chopped mint leaves.
- Add cold water and mix well.
- Add ice cubes and enjoy!

## Note

- Adjust honey and lemon juice according to your taste.
